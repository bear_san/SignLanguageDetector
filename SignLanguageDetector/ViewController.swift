//
//  ViewController.swift
//  SignLanguageDetector
//
//  Created by BearSan on 2023/03/10.
//

import UIKit
import AVFoundation
import Vision

//  参考サイト
//  https://qiita.com/mikazuki_mitsutsuki/items/6ea7acf851f1dcd66f04

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    @IBOutlet weak var previewView: UIView!
    var previewLayer: AVCaptureVideoPreviewLayer?
    var session: AVCaptureSession?
    var layers: [CALayer] = []
    
    //MARK: ハンドトラッキングのためのリクエスト
    let handTrackingRequest = VNDetectHumanHandPoseRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //MARK: キャプチャセッションの初期化
        session = .init()
        
        
        //MARK: 動画用のキャプチャデバイスの準備
        let devices = AVCaptureDevice.DiscoverySession.init(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .front).devices
        let videoDevice = devices.filter({ d in
            return d.position == .front
        }).first
        
        guard let videoDevice = videoDevice else {
            return
        }
        
        do {
            let videoInput = try AVCaptureDeviceInput(device: videoDevice)
            if session?.canAddInput(videoInput) ?? false {
                session?.addInput(videoInput)
            }
        } catch {
            fatalError("failed to initialize output device")
        }
        
        //MARK: 出力デバイス（というよりも、出力を扱うクラス）の設定
        let output = AVCaptureVideoDataOutput()
        output.setSampleBufferDelegate(self, queue: .global()) //自分自身（ViewController）
        output.videoSettings = [kCVPixelBufferPixelFormatTypeKey : kCVPixelFormatType_32BGRA] as [String : Any]
        //カラーチャンネルの設定（イメージバッファを処理する際に重要になる）
        
        if session?.canAddOutput(output) ?? false {
            session?.addOutput(output)
        }
        
        if let session = session {
            self.previewLayer = .init(session: session)
            self.previewLayer?.connection?.videoOrientation = .portrait
            self.previewLayer?.frame = self.previewView.frame
            
            self.previewView.layer.insertSublayer(self.previewLayer!, at: 0)
        }
        
        handTrackingRequest.maximumHandCount = 1
        
        //MARK: キャプチャ開始（メインスレッド以外で行う必要がある）
        DispatchQueue.global().async {
            self.session?.startRunning()
        }
        
    }
    //MARK: 映像のフレームがキャプチャされたときに呼ばれる（1フレームずつ呼ばれることになる）
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        //MARK: 指の認識
        getHandObservations(sampleBuffer: sampleBuffer)
    }
    
    private func getHandObservations(sampleBuffer: CMSampleBuffer){
        
        //MARK: イメージバッファから、指の座標を検出する
        
        let handler = VNImageRequestHandler(cmSampleBuffer: sampleBuffer, orientation: .up)
        do {
            try handler.perform([handTrackingRequest])
            guard let observation = handTrackingRequest.results?.first else {
                return
            }
            
            //MARK: 検出する指の設定（ここでは全て）
            let allPoints = try observation.recognizedPoints(.all)
            guard let indexTipPoint = allPoints[.indexTip] else {
                return
            }
            
            //MARK: 一定以下の精度の場合は無視
            guard indexTipPoint.confidence > 0.9 else{
                return
            }
            
            //MARK: 全ての丸レイヤーを一旦削除
            self.layers.forEach { l in
                DispatchQueue.main.async {
                    l.removeFromSuperlayer()
                }
            }
            self.layers = []
            
            //MARK: 表示
            let layer = CALayer()
            layer.frame = .init(origin: convertVisionPosToUIKitPos(pos: indexTipPoint),
                                size: .init(width: 100, height: 100))
            layer.borderColor = UIColor.red.cgColor
            layer.borderWidth = 1
            layer.cornerRadius = 128
            
            DispatchQueue.main.async {
                self.previewLayer?.addSublayer(layer)
            }
            self.layers.append(layer)
        } catch {
            return
        }
    }
    
    func convertVisionPosToUIKitPos(pos: VNRecognizedPoint) -> CGPoint {
        // VNRecognizedPointをCGPointに変換
        let visionCGPoint = CGPoint(x: pos.location.x, y: pos.location.y)
        
        // Vision座標をAVFoundation座標に変換
        let avFoundationPoint = CGPoint(x: visionCGPoint.x, y: 1 - visionCGPoint.y)
        
        let uiKitPoint = self.previewLayer?.layerPointConverted(fromCaptureDevicePoint: avFoundationPoint)
        if let uiKitPoint = uiKitPoint {
            return uiKitPoint
        }
        
        return .init()
    }
}

